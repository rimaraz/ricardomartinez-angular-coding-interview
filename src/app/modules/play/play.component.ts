import { Component, OnDestroy, OnInit } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { switchMap, takeUntil, tap } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit, OnDestroy {
  private _destroy$ = new Subject<void>();

  questionsAnswered =  false;
  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  error$ = this.questionsService.error$;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(switchMap(params =>
      this.questionsService.getQuestions({
        type: params.type,
        amount: params.amount,
        difficulty: params.difficulty
      })
    )).subscribe();


  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService,
  ) { }

  ngOnInit(): void {
    this.questions$.pipe(takeUntil(this._destroy$)).subscribe(qtion => {
      console.log(qtion);
      let notAswner = qtion.filter(q => !q.selectedId).length;
      console.log('notAswner: ', notAswner);
      if(notAswner === 0 && qtion.length > 0) {
        this.questionsAnswered = true;
      }
    });
  }

  onAnswerClicked(questionId: QuestionModel['_id'], answerSelected: string, answer: QuestionModel['answers']): void {
    let isCorrect = answer.find(i => i._id === answerSelected && i.isCorrect);
    console.log('isCorrect', !!isCorrect);
    this.questionsService.selectAnswer(questionId, answerSelected, !!isCorrect);
  }

  ngOnDestroy(): void {
      this._destroy$.next();
      this._destroy$.complete();
  }

}
